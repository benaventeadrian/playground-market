<?php
require_once 'functions/products.php';
require_once 'functions/utilities.php';
$products = get_products_list($db, 12);

if (isset($_SESSION['success'])) {
    $success = $_SESSION['success'];
    unset($_SESSION['success']);
} else {
    $success = "";
}
?>
<div class="container-fluid" id="home">
    <div class="container">
        <?php if (isset($success) && $success != "") : ?>
            <div class="row">
                <div class="alert alert-success w-100 mt-3" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Descartar mensaje">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="alert-heading">¡Felicitaciones!</h4>
                    <p><?= $success; ?></p>
                </div>
            </div>
        <?php endif; ?>
        <div class="row justify-content-center">
            <?php require 'components/carousel.php'; ?>
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col">
                <h2>FEN Mark</h2>
                <p class="text-light">FEN Mark es la tienda que te permite comprar los últimos videojuegos y el software más moderno en muy pocos pasos, asociando una tarjeta y usando solo tu huella digital para pagar.</p>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col">
                <h2>¡Últimas ofertas!</h2>
                <p class="text-light">Estas son algunas de las mejores ofertas que tenemos para vos</p>
            </div>
        </div>
        <div class="row mt-3 listado-productos">
            <?php
            foreach ($products as $product) :
                ?>
                <div class="col-12 col-md-6 col-lg-4 mb-4">
                    <div class="card h-100">
                        <picture>
                            <source srcset="assets/img/desktop-<?= $product['image'] ?>" media="(min-width: 1200px)">
                            <source srcset="assets/img/tablet-<?= $product['image'] ?>" media="(min-width: 768px)">
                            <img src="assets/img/phone-<?= $product['image'] ?>" class="card-img-top" alt="<?= $product['image_desc'] ?>">
                        </picture>
                        <div class="card-body">
                            <div>
                                <h4 class="card-title"><?= $product['name']; ?></h4>
                                    <p class="card-text"><?= excerpt($product['description'], 100); ?></p>

                                    <?php if ($product['price'] <= 50): ?>
                                        <div><span class="highlight-tag oferta">¡SÚPER OFERTA!</span></div>
                                    <?php endif; ?>

                                    <?php if ($product['stock'] > 0 && $product['stock'] <= 100): ?>
                                        <div><span class="highlight-tag last-units">ÚLTIMAS UNIDADES</span></div>
                                    <?php endif; ?>
                                    
                                    <?php if ($product['stock'] == 0): ?>
                                        <div><span class="highlight-tag out-of-stock">SIN STOCK</span></div>
                                    <?php endif; ?>
                                    
                            </div>
                            <a href="index.php?v=detalle&id=<?= $product['id_product'] ?>" class="btn btn-primary mt-3">Ver más</a>
                        </div>
                    </div>
                </div>
            <?php
            endforeach;
            ?>
        </div>
    </div>
</div>
