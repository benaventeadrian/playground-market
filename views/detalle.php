<?php
require "functions/products.php";
$product = get_product_by_id($db, $_GET['id']);

if (isset($_SESSION['errors'])) {
    $errors = $_SESSION['errors'];
    unset($_SESSION['errors']);
}
?>
<div class="container-fluid" id="detalle">
    <div class="container">
        <?php if (isset($errors) && $errors != "") : ?>
            <div class="row">
                <div class="alert alert-danger w-100 mt-3" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Descartar mensaje">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="alert-heading">¡Lo lamentamos!</h4>
                    <p><?= $errors; ?></p>
                </div>
            </div>
        <?php endif; ?>
        <article class="row mt-5">
            <div class="col-12 col-md-8 mb-3">
                <picture>
                    <source srcset="assets/img/desktop-<?= $product['image'] ?>" media="(min-width: 1200px)">
                    <source srcset="assets/img/tablet-<?= $product['image'] ?>" media="(min-width: 768px)">
                    <img src="assets/img/phone-<?= $product['image'] ?>" class="card-img-top" alt="<?= $product['image_desc'] ?>">
                </picture>
            </div>
            <div class="col-12 col-md-4">
                <h2><?= $product['name'] ?></h2>
                <p class="text-light"><?= $product['description'] ?></p>

                <?php if ($product['price'] <= 50) : ?>
                    <div class="mb-3">
                        <span class="highlight-tag oferta">¡SÚPER OFERTA!</span>
                    </div>
                <?php endif; ?>

                <?php if ($product['stock'] > 0 && $product['stock'] <= 100) : ?>
                    <div class="mb-3">
                        <span class="highlight-tag last-units">ÚLTIMAS UNIDADES</span>
                    </div>
                <?php endif; ?>

                <?php if ($product['stock'] == 0): ?>
                    <div class="mb-3"><span class="highlight-tag out-of-stock">SIN STOCK</span></div>
                <?php endif; ?>

                <span class="highlight text-light"><?= '$ ' . $product['price'] ?></span>
                <div class="my-4 fingerprint">
                    <img src="assets/img/fingerprint.svg" alt="Huella digital" aria-hidden="true" class="invert">
                    <p class="text-center text-light mt-3">Si estás navegando desde tu celular, apoyá tu dedo registrado sobre el sensor para pagar</p>
                </div>
                <div class="row">
                    <div class="col-12">
                        <span>Otras opciones de pago:</span>
                    </div>
                </div>
                <div class="row tarjetas">
                    <div class="col">Visa</div>
                    <div class="col">Mastercard</div>
                    <div class="col">Mercado Pago</div>
                </div>
                <?php if ($product['stock'] > 0): ?>
                    <?php if (!is_authenticated()) : ?>
                        <div class="row mt-4">
                            <div class="col">
                                <p class="alert alert-danger text-dark text-center">Inicia sesión para poder comprar</p>
                            </div>
                        </div>
                    <?php else : ?>
                        <form action="user/actions/cart/add.php" method="post">
                            <input type="hidden" name="id_product" value="<?= $product['id_product'] ?>">
                            <input type="hidden" name="id_user" value="<?= $_SESSION['active_user']['id_user']; ?>">
                            <div class="row my-4">
                                <div class="col-8">
                                    <button type="submit" class="btn btn-primary btn-block">Comprar</button>
                                </div>
                                <div class="col-4">
                                    <input type="number" name="quantity" class="form-control" min="1" max="<?= $product['stock'] ?>" value="1">
                                </div>
                            </div>
                        </form>
                    <?php endif; ?>
                <?php else : ?>
                    <p class="text-dark text-center alert alert-secondary mt-4">PRODUCTO TEMPORALMENTE NO DISPONIBLE</p>
                <?php endif; ?>
            </div>
        </article>
    </div>
</div>
