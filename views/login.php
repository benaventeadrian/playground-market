<?php
if (isset($_SESSION['errors'])) {
    $errors = $_SESSION['errors'];
    unset($_SESSION['errors']);
} else {
    $errors = [];
}
if (isset($_SESSION['old_data'])) {
    $old_data = $_SESSION['old_data'];
    unset($_SESSION['old_data']);
} else {
    $old_data = [];
}
?>
<div class="container login-panel">
    <h2 class="text-light mt-5">Identificate</h2>
    <?php
    if (isset($errors['credentials'])) :
        ?>
        <div class="text-danger">
            <p><?= $errors['credentials']; ?></p>
        </div>
    <?php
    endif;
    ?>
    <div class="row">
        <div class="col-12 col-md-6 mt-3">
            <h3>Login</h3>
            <p class="text-light mb-0">Ingresá tus credenciales</p>
            <form action="admin/actions/login.php" method="post" class="form login-form">
                <div class="form-row mt-3 mx-auto">
                    <label for="email" class="text-light">Email</label>
                    <input type="email" id="email" name="email" class="form-control" value="<?= $old_data['email'] ?? ''; ?>" placeholder="Ingresá tu email" required>
                </div>
                <div class="form-row mt-3 mx-auto">
                    <label for="password" class="text-light">Contraseña</label>
                    <input type="password" id="password" name="password" class="form-control" placeholder="Ingresá tu contraseña" required>
                </div>
                <button type="submit" class="btn btn-primary px-5 my-3">Ingresar</button>
            </form>
        </div>
        <div class="col-12 col-md-6 mt-3">
            <h3>¿No tenés cuenta?</h3>
            <p class="text-light mb-0">Creá una en tres simples pasos</p>
            <form action="admin/actions/register.php" method="post" class="form login-form">
                <div class="form-row mt-3 mx-auto">
                    <label for="email_register" class="text-light">Email</label>
                    <input type="email" id="email_register" name="email_register" class="form-control" value="<?= $old_data['email'] ?? ''; ?>" placeholder="Ingresá tu email" required>
                    <?php if (isset($errors['email_register'])) : ?>
                        <div class="form-error" aria-describedby="email_register"><?= $errors['email_register']; ?> </div>
                    <?php endif; ?>
                </div>
                <div class="form-row mt-3 mx-auto">
                    <label for="password_register" class="text-light">Contraseña</label>
                    <input type="password" id="password_register" name="password_register" class="form-control" minlength="3" placeholder="Ingresá tu contraseña" required>
                    <?php if (isset($errors['password_register'])) : ?>
                        <div class="form-error" aria-describedby="password_register"><?= $errors['password_register']; ?> </div>
                    <?php endif; ?>
                </div>
                <div class="form-row mt-3 mx-auto">
                    <label for="password_repeat" class="text-light">Repetir contraseña</label>
                    <input type="password" id="password_repeat" name="password_repeat" class="form-control" placeholder="Volvé a ingresar tu contraseña" required>
                    <?php if (isset($errors['password_repeat'])) : ?>
                        <div class="form-error" aria-describedby="password_repeat"><?= $errors['password_repeat']; ?> </div>
                    <?php endif; ?>
                </div>
                <button type="submit" class="btn btn-primary px-5 my-3">Ingresar</button>
            </form>
        </div>
    </div>
</div>
