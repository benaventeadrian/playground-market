<div class="container-fluid" class="mantenimiento">
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-6 col-lg-8">
                <h2>Mantenimiento</h2>
                <p class="text-light">¡Lo sentimos! Este sitio se encuentra en mantenimiento, estamos trabajando para mejorar tu experiencia.</p>
            </div>
            <div class="col-md-6 col-lg-4 mb-4">
                <img src="assets/img/vault_boy_out_of_order.png" class="d-block mx-auto" alt="Vault Boy descompuesto">
            </div>
        </div>
    </div>
</div>
