<?php
require_once "functions/user.php";
require_once "functions/utilities.php";

if (isset($_SESSION['errors'])) {
    $errors = $_SESSION['errors'];
    unset($_SESSION['errors']);
} else {
    $errors = '';
}

if (!is_authenticated()) {
    header("Location: index.php?v=home");
    exit;
}

$user_data = get_user_by_id($db, $_SESSION['active_user']['id_user']);

?>
<div class="container-fluid" id="user-profile">
    <div class="container">
        <div class="row mt-5">
            <div class="col-12">
                <h2>Mi Perfil</h2>
            </div>
        </div>
        <div class="row mt-5 justify-content-center">
            <div class="col">
                <form action="user/actions/edit-profile.php" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div>
                                <label for="name" class="text-light">Nombre:</label>
                            </div>
                            <div class="user-attributes-wrapper">
                                <?php if (!$user_data['name'] && $user_data['name'] == "") : ?>
                                    <p class="text-light">Sin definir</p>
                                <?php endif; ?>
                                <input type="text" name="name" value="<?= $user_data['name']; ?>" class="form-control" id="name" readonly>
                            </div>

                            <?php if (isset($errors['name'])) : ?>
                                <div class="form-error" aria-describedby="name"><?= $errors['name']; ?> </div>
                            <?php endif; ?>

                            <div class="mb-4">
                                <a role="button" class="btn btn-primary mt-3" id="edit-name">Editar</a>
                            </div>
                            <div>
                                <label for="last_name" class="text-light">Apellido:</label>
                            </div>
                            <div class="user-attributes-wrapper">
                                <?php if (!$user_data['last_name'] && $user_data['last_name'] == "") : ?>
                                    <p class="text-light">Sin definir</p>
                                <?php endif; ?>
                                <input type="text" name="last_name" value="<?= $user_data['last_name']; ?>" class="form-control" id="last_name" readonly>
                            </div>

                            <?php if (isset($errors['last_name'])) : ?>
                                <div class="form-error" aria-describedby="last_name"><?= $errors['last_name']; ?> </div>
                            <?php endif; ?>

                            <div class="mb-4">
                                <a role="button" class="btn btn-primary mt-3" id="edit-lastname">Editar</a>
                            </div>
                            <div>
                                <label for="email" class="text-light">Email:</label>
                            </div>
                            <div>
                                <input type="text" value="<?= $user_data['email'] ?>" id="email" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="avatar" class="text-light">Avatar:</label>
                            <div>
                                <?php if ($user_data['avatar'] && $user_data['avatar'] != "") : ?>
                                    <img src="assets/img/users/avatars/<?= $user_data['avatar']; ?>" alt="Avatar de <?= $user_data['avatar'] ?? 'usuario'; ?>" class="avatar" />
                                <?php else : ?>
                                    <img src="assets/img/default_avatar.png" alt="Avatar por defecto" class="avatar" />
                                <?php endif; ?>
                                <div class="mt-3">
                                    <input type="file" name="avatar" id="avatar" aria-label="Editar avatar">
                                </div>
                                <?php if (isset($errors['avatar'])) : ?>
                                    <div class="form-error" aria-describedby="avatar"><?= $errors['avatar']; ?> </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-4 text-center mx-auto mt-5 mb-5">
                            <button type="submit" class="btn btn-secondary btn-block btn-lg">Guardar cambios</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
