<?php
if (isset($_SESSION['success'])) {
    $success = $_SESSION['success'];
    unset($_SESSION['success']);
}

if (isset($_SESSION['errors'])) {
    $errors = $_SESSION['errors'];
    unset($_SESSION['errors']);
} else {
    $errors = [];
}

?>
<div class="container-fluid" id="contacto">
    <div class="container">
        <?php if (isset($success) && $success != "") : ?>
            <div class="row">
                <div class="alert alert-success w-100 mt-3" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Descartar mensaje">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="alert-heading">¡Felicitaciones!</h4>
                    <p><?= $success; ?></p>
                </div>
            </div>
        <?php endif; ?>
        <div class="row mt-5">
            <div class="col-12">
                <h2>Contacto</h2>
            </div>
        </div>
        <div class="row">
            <div class="col col-md-6">
                <form action="user/actions/contact/send.php" method="post" class="form form-contact">
                    <div class="form-row">
                        <label for="email" class="text-light">Dirección de e-mail *</label>
                        <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Ingresá tu e-mail" value="<?= $old_data['email'] ?? ''; ?>">
                        <?php if (isset($errors['email'])) : ?>
                            <div class="form-error w-100" aria-describedby="email"><?= $errors['email']; ?> </div>
                        <?php endif; ?>
                        <small id="emailHelp" class="form-text text-muted">No lo compartiremos con nadie más.</small>
                    </div>
                    <div class="form-row">
                        <label for="name" class="text-light">Nombre *</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Ingresá tu nombre" value="<?= $old_data['name'] ?? ''; ?>">
                        <?php if (isset($errors['name'])) : ?>
                            <div class="form-error" aria-describedby="name"><?= $errors['name']; ?> </div>
                        <?php endif; ?>
                    </div>
                    <div class="form-row">
                        <label for="message" class="text-light">Dejanos tu consulta *</label>
                        <textarea class="form-control" name="message" id="message" rows="3"><?= $old_data['message'] ?? ''; ?></textarea>
                        <?php if (isset($errors['message'])) : ?>
                            <div class="form-error" aria-describedby="message"><?= $errors['message']; ?> </div>
                        <?php endif; ?>
                    </div>
                    <small class="form-text text-warning">* Campos requeridos</small>
                    <button type="submit" class="btn btn-primary px-5 my-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
