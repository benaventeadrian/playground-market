<?php
require_once "functions/utilities.php";
require_once "functions/products.php";
require_once "functions/cart.php";

$products = get_user_cart($db, $_SESSION['active_user']['id_user']);

if (isset($_SESSION['errors'])) {
    $errors = $_SESSION['errors'];
    unset($_SESSION['errors']);
}

if (!is_authenticated()) {
    header("Location: index.php?v=home");
}
?>
<div class="container-fluid" id="cart">
    <div class="container">
        <?php if (isset($errors) && $errors != "") : ?>
            <div class="row">
                <div class="alert alert-danger w-100 mt-3" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Descartar mensaje">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="alert-heading">¡Lo lamentamos!</h4>
                    <p><?= $errors; ?></p>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-12 mt-5">
                <h2>Carrito</h2>
            </div>
        </div>
        <?php if (empty($products)) : ?>
            <div class="row">
                <div class="col-md-6 col-lg-8">
                    <p class="text-light">¡Vaya! Parece que no has agregado ningún producto aún. ¿Qué esperás para darle vida a este carrito de compras? Te invitamos a navegar <a href="index.php?v=productos">nuestra lista de productos</a>, seguro encontrarás alguno que te interese.</p>
                </div>
                <div class="col-md-6 col-lg-4 mb-4">
                    <img src="assets/img/vault_boy_sad.png" class="d-block mx-auto" alt="Vault Boy triste porque aun no agregaste nada al carrito">
                </div>
            </div>
        <?php else : ?>
            <div class="row">
                <?php
                    foreach ($products as $product) :
                        $this_product = get_product_by_id($db, $product['id_product']);
                        ?>
                    <div class="col-sm-6">
                        <div class="card mb-3">
                            <div class="row no-gutters">
                                <div class="col-md-4">
                                    <picture>
                                        <source srcset="assets/img/desktop-<?= $this_product['image']; ?>" media="(min-width: 1200px)">
                                        <source srcset="assets/img/tablet-<?= $this_product['image']; ?>" media="(min-width: 768px)">
                                        <img src="assets/img/phone-<?= $this_product['image']; ?>" class="card-img thumbnail" alt="<?= $this_product['image_desc']; ?>">
                                    </picture>
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h3 class="card-title text-dark"><?= $this_product['name']; ?></h3>
                                        <p class="card-text"><small class="text-muted">Cantidad: <?= $product['total'] ?></small></p>
                                        <p class="card-text">Total: $<?= $this_product['price'] * $product['total']; ?></p>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <form action="user/actions/cart/remove.php" method="post">
                                                    <input type="hidden" name="id_user" value="<?= $_SESSION['active_user']['id_user'] ?>">
                                                    <input type="hidden" name="id_product" value="<?= $product['id_product'] ?>">
                                                    <button type="submit" class="btn btn-danger btn-block remove-item" aria-label="Quitar item del carrito">
                                                        <span aria-hidden="true" class="icon icon-clearclose"></span>
                                                        Eliminar
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                    endforeach;
                    ?>
            </div>
            <div class="row">
                <div class="col-md-4 text-center mx-auto mt-5 mb-5">
                    <a role="button" class="btn btn-secondary btn-block btn-lg" href="index.php?v=comprar">Realizar pedido</a>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
