<?php
require_once "functions/utilities.php";
require_once "functions/cart.php";

$id_user = $_SESSION['active_user']['id_user'];
$cart = get_user_cart($db, $id_user);
$json = json_decode(file_get_contents("assets/json/provincias.json"));
$provincias = $json->provincias;

if (isset($_SESSION['errors'])) {
    $errors = $_SESSION['errors'];
    unset($_SESSION['errors']);
} else {
    $errors = [];
}

if (isset($_SESSION['old_data'])) {
    $old_data = $_SESSION['old_data'];
    unset($_SESSION['old_data']);
} else {
    $old_data = [];
}

if (!is_authenticated() || empty($cart)) {
    header("Location: index.php?v=home");
}
?>
<div class="container-fluid" id="purchase">
    <div class="container">
        <div class="row">
            <div class="col-12 mt-5">
                <h2>Confirmá tu pedido</h2>
                <p class="text-light">¿A dónde te lo enviamos?</p>
            </div>
        </div>
        <div class="row text-light">
            <div class="col-md-6">
                <form action="user/actions/cart/confirm.php" method="post">
                    <input type="hidden" name="id_user" value="<?= $id_user; ?>">

                    <?php foreach ($cart as $item) : ?>
                        <input type="hidden" name="id_product[]" value="<?= $item['id_product']; ?>">
                        <input type="hidden" name="quantity[]" value="<?= $item['total']; ?>">
                    <?php endforeach; ?>

                    <div class="form-group">
                        <div><label for="address">Dirección</label></div>
                        <div><input type="text" name="address" id="address" class="form-control" placeholder="Ej: Av. Corrientes 2037" value="<?= $old_data['address'] ?? ''; ?>"></div>
                        <?php if (isset($errors['address'])) : ?>
                            <div class="form-error" aria-describedby="address"><?= $errors['address']; ?> </div>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <div><label for="city">Ciudad</label></div>
                        <div><input type="text" name="city" id="city" class="form-control" placeholder="Ej: Rosario" value="<?= $old_data['city'] ?? ''; ?>"></div>
                        <?php if (isset($errors['city'])) : ?>
                            <div class="form-error" aria-describedby="city"><?= $errors['city']; ?> </div>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <div><label for="zip_code">Código Postal</label></div>
                        <div><input type="text" name="zip_code" id="zip_code" class="form-control" placeholder="Ej: 1414 ó C1414BZD" value="<?= $old_data['zip_code'] ?? ''; ?>"></div>
                        <?php if (isset($errors['zip_code'])) : ?>
                            <div class="form-error" aria-describedby="zip_code"><?= $errors['zip_code']; ?> </div>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <div><label for="province">Provincia</label></div>
                        <div>
                            <select name="province" id="province" class="form-control">
                                <option value="">Seleccioná una provincia</option>
                                <?php foreach ($provincias as $provincia) : ?>
                                    <option value="<?= $provincia->iso_nombre; ?>">
                                        <?= $provincia->iso_nombre; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <?php if (isset($errors['province'])) : ?>
                            <div class="form-error" aria-describedby="province"><?= $errors['province']; ?> </div>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <div>
                            <fieldset>
                                <legend>Forma de pago</legend>
                                <div class="field-group payment-set">
                                    <div class="method visa">
                                        <input type="radio" name="payment_method" id="visa" value="visa">
                                        <label for="visa">Visa</label>
                                    </div>
                                    <div class="method mastercard">
                                        <input type="radio" name="payment_method" id="mastercard" value="mastercard">
                                        <label for="mastercard">Mastercard</label>
                                    </div>
                                    <div class="method mercadopago">
                                        <input type="radio" name="payment_method" id="mercadopago" value="mercadopago">
                                        <label for="mercadopago">Mercado Pago</label>
                                    </div>
                                </div>
                            </fieldset>
                            <?php if (isset($errors['payment_method'])) : ?>
                                <div class="form-error" aria-describedby="payment_method"><?= $errors['payment_method']; ?> </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="my-4"><button type="submit" class="btn btn-block btn-secondary">Comprar</button></div>
                </form>
            </div>
            <div class="col-md-6">
                <img src="assets/img/vault_boy_delivery.png" class="d-block mx-auto" alt="Vault Boy listo para enviar tu producto a donde nos digas">
            </div>
        </div>
    </div>
</div>
