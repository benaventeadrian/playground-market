<div class="container">
    <div class="row mt-5">
        <div class="col-md-6 col-lg-8">
            <h2>Oops, nada por aquí!</h2>
            <p class="text-light">Te invitamos a revisar <a href="index.php?v=productos" class="text-primary">nuestra lista de productos</a>, quizás allí encuentres lo que estás buscando.</p>
        </div>
        <div class="col-md-6 col-lg-4 mb-4">
            <img src="assets/img/vault_boy_moving.png" class="d-block mx-auto" alt="Vault Boy yéndose de aquí silbando bajito">
        </div>
    </div>
</div>
