<?php
    require_once "functions/products.php";
    require_once "functions/utilities.php";

    $categories = [];
    $categories["Accion"] = get_products_by_category($db, "Acción");
    $categories["Aventura"] = get_products_by_category($db, "Aventura");
    $categories["Deportes"] = get_products_by_category($db, "Deportes");
    $categories["Estrategia"] = get_products_by_category($db, "Estrategia");
    $categories["Indie"] = get_products_by_category($db, "Indie");
    $categories["RPG"] = get_products_by_category($db, "RPG");
    $categories["Shooter"] = get_products_by_category($db, "Shooter");
    $categories["Survival Horror"] = get_products_by_category($db, "Survival Horror");
?>
<div class="row mt-3 listado-productos">
    <div class="col">
        <?php foreach ($categories as $category): ?>
            <section class="row">
                <div class="col-12 mt-3">
                    <h3><?= $category[0]['category'] ?? ''; ?></h3>
                </div>
                <?php for ($i = 0;$i < count($category);$i++): ?>
                    <div class="col-12 col-md-6 col-lg-4 mb-4">
                        <div class="card h-100">
                            <picture>
                                <source srcset="assets/img/desktop-<?= $category[$i]['image'] ?>" media="(min-width: 1200px)">
                                <source srcset="assets/img/tablet-<?= $category[$i]['image'] ?>" media="(min-width: 768px)">
                                <img src="assets/img/phone-<?= $category[$i]['image'] ?>" class="card-img-top" alt="<?=$category[$i]['image_desc']?>">
                            </picture>
                            <div class="card-body">
                                <div>
                                    <h4 class="card-title"><?= $category[$i]['name']; ?></h4>
                                    <p class="card-text"><?= excerpt($category[$i]['description'], 100); ?></p>

                                    <?php if ($category[$i]['price'] <= 50): ?>
                                        <div><span class="highlight-tag oferta">¡SÚPER OFERTA!</span></div>
                                    <?php endif; ?>

                                    <?php if ($category[$i]['stock'] > 0 && $category[$i]['stock'] <= 100): ?>
                                        <div><span class="highlight-tag last-units">ÚLTIMAS UNIDADES</span></div>
                                    <?php endif; ?>
                                    
                                    <?php if ($category[$i]['stock'] == 0): ?>
                                        <div><span class="highlight-tag out-of-stock">SIN STOCK</span></div>
                                    <?php endif; ?>

                                </div>

                                <a href="index.php?v=detalle&id=<?=$category[$i]['id_product']?>" class="btn btn-primary mt-3">Ver más</a>
                                
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
            </section>
        <?php endforeach; ?>
    </div>
</div>
