<?php
if (isset($_SESSION['success'])) {
    $success = $_SESSION['success'];
    unset($_SESSION['success']);
}
?>
<div class="container-fluid" id="productos">
    <div class="container">
        <?php if (isset($success) && $success != "") : ?>
            <div class="row">
                <div class="alert alert-success w-100 mt-3" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Descartar mensaje">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="alert-heading">¡Felicitaciones!</h4>
                    <p><?= $success; ?></p>
                </div>
            </div>
        <?php endif; ?>
        <div class="row mt-5">
            <div class="col-12">
                <h2>Productos</h2>
                <p class="text-light">Tenemos las mejores ofertas para vos, ¡apurate antes de que se esfumen!</p>
                <p class="text-light">Si ves algún producto con el tag "SÚPER OFERTA", aprovechá, se trata de un descuento por tiempo limitado con rebajas de hasta el 50%.</p>
            </div>
        </div>
    </div>
    <div class="container">
        <?php require 'listado.php'; ?>
    </div>
</div>
