<?php
session_start();
require "../../../functions/mail.php";

$email = $_POST['email'];
$name = $_POST['name'];
$message = $_POST['message'];

$errors = [];

if ($email == "") {
    $errors['email'] = "El email no puede estar vacío.";
} else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $errors['email'] = "El email no parece tener un formato válido.";
}

if ($name == "") {
    $errors['name'] = "El nombre no puede estar vacío.";
} else if (strlen($name) > 1000) {
    $errors['name'] = "Nombre demasiado largo.";
}

if ($message == "") {
    $errors['message'] = "El mensaje no puede estar vacío.";
} else if (strlen($message) > 5000) {
    $errors['message'] = "Mensaje demasiado largo.";
}

if (count($errors) > 0) {
    $_SESSION['old_data'] = $_POST;
    $_SESSION['errors'] = $errors;

    header('Location: ../../../index.php?v=contacto');
    exit;
}

if (email_contact($email)) {
    $_SESSION['success'] = "Mensaje enviado exitosamente, te responderemos a la brevedad";
    header('Location: ../../../index.php?v=contacto');
} else {
    $_SESSION['errors'] = "Tuvimos un problema para procesar tu mensaje. Por favor intentalo de nuevo más tarde";
    header('Location: ../../../index.php?v=contacto');
}
