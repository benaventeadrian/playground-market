<?php
require "../../bootstrap/connection.php";
require "../../functions/user.php";
require "../../functions/image.php";

if (!isset($_POST) || empty($_POST)) {
    header("Location: ../../index.php?v=perfil");
    exit;
} else {
    $user = $_SESSION['active_user']['id_user'];
    $name = $_POST['name'] ?? "";
    $last_name = $_POST['last_name'] ?? "";

    $errors = [];

    if (strlen($name) > 500) {
        $errors['name'] = "El nombre no puede tener más de 500 caracteres.";
    } else {
        $success = set_user_name($db, $user, $name);
    }

    if (strlen($last_name) > 1000) {
        $errors['last_name'] = "El apellido no puede tener más de 1000 caracteres.";
    } else {
        $success = set_user_lastname($db, $user, $last_name);
    }

    if (isset($_FILES) && !empty($_FILES['avatar']['name'])) {
        $avatar_path = "../../assets/img/users/avatars/";
        $avatar_name = get_filename($_FILES['avatar']['name']);
        $avatar_extension = get_extension($_FILES['avatar']['name']);
        $avatar_filename = $avatar_name . "." . $avatar_extension;
        $avatar_tmp = $_FILES['avatar']['tmp_name'];
        $avatar_size = $_FILES['avatar']['size'];
        $valid_extensions = ["jpg", "jpeg", "webp", "gif", "png"];

        if ($avatar_size > 5242880) {
            $errors['avatar'] = "El avatar no puede pesar más de 5 megabytes";
        } else if (strlen($avatar_name) > 5000) {
            $errors['avatar'] = "El nombre del archivo no puede tener más de 5000 caracteres";
        } else if (!in_array($avatar_extension, $valid_extensions)) {
            $errors['avatar'] = "Formato de archivo inválido. Solo se admiten .jpg, .jpeg, .webp, .git y .png";
        } else if (empty($avatar_tmp)) {
            $errors['avatar'] = "Error temporal del servidor. Por favor, reintentá más tarde.";
        } else {
            if (move_uploaded_file($avatar_tmp, $avatar_path . $avatar_filename)) {
                $success_avatar = set_user_avatar($db, $user, $avatar_filename);
            }
        }
    }

    if (count($errors) > 0) {
        $_SESSION['errors'] = $errors;
        header("Location:  ../../index.php?v=perfil");
        exit;
    }

    if($success || $success_avatar) {
        header("Location: ../../index.php?v=perfil");
    } else {
        $_SESSION['errors'] = "Error del servidor. Por favor, intentalo de nuevo más tarde.";
        header("Location: ../../index.php?v=perfil");
    }
}
