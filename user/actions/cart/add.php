<?php

require "../../../bootstrap/connection.php";
require "../../../functions/products.php";
require "../../../functions/cart.php";

$id_product = $_POST['id_product'];
$quantity = $_POST['quantity'];
$id_user = $_POST['id_user'];

if (!add_to_cart($db, $id_product, $quantity, $id_user)) {
    $_SESSION['errors'] = "Error del servidor. Por favor, intentalo de nuevo más tarde.";
    header("Location: ../../../index.php?v=detalle&id=". $id_product);
} else {
    $_SESSION['success'] = "El producto fue agregado al carrito";
    header("Location: ../../../index.php?v=productos");
}
