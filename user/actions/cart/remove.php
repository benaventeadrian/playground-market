<?php

require "../../../bootstrap/connection.php";
require "../../../functions/cart.php";

$id_user = $_POST['id_user'];
$id_product = $_POST['id_product'];

if(!remove_from_cart($db, $id_user, $id_product)) {
    $_SESSION['errors'] = "Error temporal del servidor. Estamos trabajando para solucionarlo.";
}

header("Location: ../../../index.php?v=carrito");
