<?php

require "../../../bootstrap/connection.php";
require "../../../functions/cart.php";
require "../../../functions/orders.php";
require "../../../functions/mail.php";

$id_user = $_POST['id_user'];
$id_product = $_POST['id_product'];
$quantity = $_POST['quantity'];
$address = $_POST['address'];
$city = $_POST['city'];
$zip_code = $_POST['zip_code'];
$province = $_POST['province'];
$payment_method = $_POST['payment_method'];

$errors = [];

if ($address == "") {
    $errors['address'] = "El campo dirección no puede estar vacío";
} else if (strlen($address) < 3) {
    $errors['address'] = "Dirección demasiado corta";
} else if (strlen($address) > 5000) {
    $errors['address'] = "Dirección demasiado larga";
}

if ($city == "") {
    $errors['city'] = "El campo ciudad no puede estar vacío";
} else if (strlen($city) < 2) {
    $errors['city'] = "Nombre de ciudad demasiado corto";
} else if (strlen($city) > 5000) {
    $errors['city'] = "Nombre de ciudad demasiado largo";
}

if ($zip_code == "") {
    $errors['zip_code'] = "El campo código postal no puede estar vacío";
} else if (strlen($zip_code) > 100) {
    $errors['zip_code'] = "Código postal demasiado largo";
}

if ($province == "") {
    $errors['province'] = "El campo provincia no puede estar vacío";
}

if ($payment_method == "") {
    $errors['payment_method'] = "Debes seleccionar un método de pago.";
}

if (count($errors) > 0) {
    $_SESSION['old_data'] = $_POST;
    $_SESSION['errors'] = $errors;

    header("Location: ../../../index.php?v=comprar");
    exit;
}

for ($i = 0; $i < count($id_product); $i++) {
    $order = create_order($db, [
        'id_user' => $id_user,
        'id_product' => $id_product[$i],
        'quantity' => $quantity[$i],
        'address' => $address,
        'city' => $city,
        'zip_code' => $zip_code,
        'province' => $province,
        'payment_method' => $payment_method
    ]);

    $stock = update_stock($db, $id_product[$i], $quantity[$i]);
}

if ($order && $stock && remove_from_cart($db, $id_user) && email_order_details($db, $_POST)) {
    $_SESSION['success'] = "Estamos procesando tu pedido. Te hemos enviado un correo con los detalles de tu compra";
    header("Location: ../../../index.php?v=home");
} else {
    $_SESSION['errors'] = "Error del servidor. Por favor, intentalo de nuevo más tarde";
    header("Location: ../../../index.php?v=comprar");
}
