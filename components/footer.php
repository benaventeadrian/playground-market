<footer>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p class="text-light">
                    &copy; <?= date('Y'); ?> - Playground
                </p>
                <ul class="text-light">
                    <li>Materia: Programación 2</li>
                    <li>Comisión: DWN3A</li>
                    <li>Alumno: BENAVENTE, Adrián</li>
                    <li>Profesor: GALLINO, Santiago</li>
                    <li>Caracter de entrega: Final</li>
                    <li>Repositorio: <a href="https://gitlab.com/benaventeadrian/playground-market" target="_blank" class="text-warning repository">https://gitlab.com/benaventeadrian/playground-market</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
