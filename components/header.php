<?php
require_once 'functions/utilities.php';
?>
<header class="container-fluid">
    <div class="container">
        <button type="button" class="burger">Menu</button>
        <h1 id="logo"><a href="index.php?v=home">Playgroundet</a></h1>
        <nav class="main-menu">
            <div class="container">
                <ul>
                    <li><a href="index.php?v=home" <?= $view == 'home' ? 'class="active"' : '' ?>>Home</a></li>
                    <li><a href="index.php?v=productos" <?= $view == 'productos' ? 'class="active"' : '' ?>>Productos</a></li>
                    <li><a href="index.php?v=contacto" <?= $view == 'contacto' ? 'class="active"' : '' ?>>Contacto</a></li>
                    <?php if (!is_authenticated()) : ?>
                        <li><a href="index.php?v=login" <?= $view == 'login' ? 'class="active"' : '' ?>>Acceso</a></li>
                    <?php endif; ?>
                    <?php 
                    if (is_authenticated()) : 
                    require_once "functions/cart.php";
                    ?>
                        <li class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Cuenta
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="index.php?v=perfil">Perfil</a>
                                <a class="dropdown-item" href="admin/actions/logout.php">Cerrar sesión</a>
                            </div>
                        </li>
                        <li class="cart"><a href="index.php?v=carrito"><span class="icon icon-shopping_cart <?php if (get_user_cart($db, $_SESSION['active_user']['id_user'])) echo 'has-items'; ?>"></span></a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </nav>
    </div>
</header>
