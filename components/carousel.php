
<?php
$n_of_products = count_products($db);
$slide1 = get_product_by_id($db, rand(1, $n_of_products));
$slide2 = get_product_by_id($db, rand(1, $n_of_products));
$slide3 = get_product_by_id($db, rand(1, $n_of_products));
?>
<div id="carouselHome" class="carousel slide carousel-fade" data-ride="carousel">
	<div class="carousel-inner">
        <div class="carousel-item active" data-interval="5000">
            <a href="index.php?v=detalle&id=<?= $slide1['id_product'] ?>">
                <picture>
                    <source srcset="assets/img/desktop-<?= $slide1['image'] ?>" media="(min-width: 1200px)">
                    <source srcset="assets/img/tablet-<?= $slide1['image'] ?>" media="(min-width: 768px)">
                    <img src="assets/img/phone-<?= $slide1['image'] ?>" class="d-block w-100" alt="<?=$slide1['image_desc']?>">
                </picture>
                <div class="slide-text">
                    <h2><?= $slide1['name'] ?></h2>
                    <p><?= $slide1['description'] ?></p>
                </div>
            </a>
        </div>
        <div class="carousel-item" data-interval="5000">
            <a href="index.php?v=detalle&id=<?= $slide2['id_product'] ?>">
                <picture>
                    <source srcset="assets/img/desktop-<?= $slide2['image'] ?>" media="(min-width: 1200px)">
                    <source srcset="assets/img/tablet-<?= $slide2['image'] ?>" media="(min-width: 768px)">
                    <img src="assets/img/phone-<?= $slide2['image'] ?>" class="d-block w-100" alt="<?=$slide2['image_desc']?>">
                </picture>
                <div class="slide-text">
                    <h2><?= $slide2['name'] ?></h2>
                    <p><?= $slide2['description'] ?></p>
                </div>
            </a>
        </div>
        <div class="carousel-item" data-interval="5000">
            <a href="index.php?v=detalle&id=<?= $slide3['id_product'] ?>">
                <picture>
                    <source srcset="assets/img/desktop-<?= $slide3['image'] ?>" media="(min-width: 1200px)">
                    <source srcset="assets/img/tablet-<?= $slide3['image'] ?>" media="(min-width: 768px)">
                    <img src="assets/img/phone-<?= $slide3['image'] ?>" class="d-block w-100" alt="<?=$slide3['image_desc']?>">
                </picture>
                <div class="slide-text">
                    <h2><?= $slide3['name'] ?></h2>
                    <p><?= $slide3['description'] ?></p>
                </div>
            </a>
        </div>
	</div>
	<a class="carousel-control-prev" href="#carouselHome" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#carouselHome" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
