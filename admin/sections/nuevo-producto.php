<?php
require '../functions/products.php';
$categories = get_categories($db);

if(isset($_SESSION['errors'])) {
    $errors = $_SESSION['errors'];
    unset($_SESSION['errors']);
}

if(isset($_SESSION['old_data'])) {
    $old_data = $_SESSION['old_data'];
    unset($_SESSION['old_data']);
} else {
    $old_data = [];
}
?>
<div class="container">
    <?php if (isset($errors) && $errors != "" && !is_array($errors)) : ?>
        <div class="row">
            <div class="alert alert-danger w-100 mt-3" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Descartar mensaje">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="alert-heading">¡Lo lamentamos!</h4>
                
                <p><?= $errors; ?></p>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-12 mt-5">
            <h2 class="text-light">Agregar un nuevo producto</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form action="actions/create.php" method="post" enctype="multipart/form-data" class="form form-crud" id="formCreateProduct">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-row">
                            <label for="title" class="text-light">Título del juego</label>
                            <input type="text" id="title" name="title" placeholder="Ingresá el título del juego" class="form-control" minlength="3" maxlength="500" required value="<?= $old_data['title'] ?? '';?>">
                            <?php
                            if(isset($errors['title'])):
                            ?>
                            <div class="form-error" aria-describedby="title"><?= $errors['title'];?></div>
                            <?php
                            endif;
                            ?>
                        </div>
                        <div class="form-row">
                            <label for="description" class="text-light">Descripción del juego</label>
                            <textarea id="description" name="description" placeholder="Agregá una breve descripción del juego" class="form-control" cols="50" rows="10" minlength="10" maxlength="2000" required><?= $old_data['description'] ?? '';?></textarea>
                            <?php
                            if(isset($errors['description'])):
                            ?>
                            <div class="form-error" aria-describedby="description"><?= $errors['description'];?></div>
                            <?php
                            endif;
                            ?>
                        </div>
                        <div class="form-row">
                            <label for="price" class="text-light">Precio</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input type="text" id="price" name="price" placeholder="Agregá el valor en pesos del juego" class="form-control" pattern="\d*" maxlength="6" required value="<?= $old_data['price'] ?? '';?>">
                                <?php
                                if(isset($errors['price'])):
                                ?>
                                <div class="form-error" aria-describedby="price"><?= $errors['price'];?></div>
                                <?php
                                endif;
                                ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <label for="stock" class="text-light">Stock</label>
                            <input type="text" id="stock" name="stock" placeholder="Unidades en stock" class="form-control" pattern="\d*" maxlength="8" required value="<?= $old_data['stock'] ?? '';?>">
                            <?php
                            if(isset($errors['stock'])):
                            ?>
                            <div class="form-error" aria-describedby="stock"><?= $errors['stock'];?></div>
                            <?php
                            endif;
                            ?>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-row">
                            <label for="category" class="text-light">Categoría</label>
                            <select name="category" id="category" class="form-control" required>
                                <option value="" selected>Seleccioná una categoría</option>
                                <?php
                                foreach ($categories as $category):
                                ?>
                                <option value="<?= $category['id_category']; ?>"><?= $category['name']; ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                            <?php
                            if(isset($errors['category'])):
                            ?>
                            <div class="form-error" aria-describedby="category"><?= $errors['category'];?></div>
                            <?php
                            endif;
                            ?>
                        </div>
                        <div class="form-row">
                            <input type="file" id="cover" class="sr-only" name="cover" accept="image/*">
                            <label for="cover" class="text-light">Portada del producto</label>
                        </div>
                        <div class="form-row">
                            <label for="cover_desc" class="text-light">Texto alternativo de la imagen</label>
                            <input type="text" id="cover_desc" name="cover_desc" placeholder="Breve descripción de la imagen para lectores de pantalla" class="form-control" value="<?= $old_data['cover_desc'] ?? '';?>">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary px-5 my-3">Agregar</button>
            </form>
        </div>
    </div>
</div>
