<?php
require '../functions/products.php';
require '../functions/orders.php';
$latest_products = get_latest_products($db, 6);
$latest_orders = get_latest_orders($db, 6);
?>

<div class="container admin-home">
    <div class="row">
        <div class="col-12 mt-5 mb-5">
            <h1>Bienvenido/a</h1>
            <p class="text-light"><?= $_SESSION['active_user']['email']; ?></p>
        </div>
    </div>
    <div class="row justify-content-between align-items-end vault-boy">
        <div class="col-lg-6 col-xl-8">
            <div class="row">
                <div class="col-lg-6 mb-4">
                    <h2>Últimos títulos agregados:</h2>
                    <div class="mt-3">
                        <ul class="latest-items">
                            <?php
                                foreach ($latest_products as $product) :
                            ?>
                                <li class="item"><img src="../assets/img/phone-<?= $product['image']; ?>" alt="<?= $product['image_desc']; ?>" class="thumbnail"><?= $product['name']; ?></li>
                            <?php
                                endforeach;
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <h2>Nuevos pedidos:</h2>
                    <div class="mt-3">
                        <ul class="latest-orders">
                            <?php
                                date_default_timezone_set("America/Buenos_Aires");
                                setlocale(LC_TIME, "es_ES");
                                foreach ($latest_orders as $order) :
                            ?>
                                <li class="text-light order mb-3"><?= $order['user'] . ": " . $order['game'] . ", el " . strftime("%d de %B de %G", strtotime($order['date'])); ?></li>
                            <?php
                                endforeach;
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4">
            <img src="../assets/img/vault_boy.png" class="d-block mx-auto" alt="Vault Boy dándote la Bienvenida">
        </div>
    </div>
</div>
