<?php
if (isset($_SESSION['success'])) {
    $success = $_SESSION['success'];
    unset($_SESSION['success']);
} else {
    $success = '';
}

if (isset($_SESSION['error'])) {
    $error = $_SESSION['error'];
    unset($_SESSION['error']);
} else {
    $error = '';
}
?>
<div class="container-fluid" id="productos">
  <div class="container">
    <div class="row mt-5">
      <div class="col-12">
        <?php
        if ($success !== ''):
        ?>
        <p class="success-message text-success"><?= $success; ?></p>
        <?php
        endif;
        ?>
        <?php
        if ($error !== ''):
        ?>
        <p class="error-message text-danger"><?= $error; ?></p>
        <?php
        endif;
        ?>
        <h2>Productos</h2>
        <p class="text-light">Este es el listado de todos los productos cargados actualmente separados por categoría.</p>
      </div>
    </div>
  </div>
  <div class="container">
    <?php require 'listado.php'; ?>
  </div>
</div>
