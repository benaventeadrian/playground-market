<?php
require '../functions/products.php';
$product = get_product_by_id($db, $_GET['id']);
?>

<div class="container">
    <div class="row">
        <div class="col-12 mt-5">
            <h2 class="text-light">Eliminar un producto</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-lg-6">
            <p class="text-light">Estás a punto de borrar el producto <strong><?= $product['name']; ?></strong>, esta acción no se puede deshacer. Presioná <strong>"Confirmar"</strong> si estás seguro o cancelar para volver a la página anterior.</p>
            <div>
                <a href="index.php?v=productos" class="btn btn-primary mt-3 mr-2">Cancelar</a>
              <a href="actions/delete.php?id=<?=$_GET['id']?>" class="btn btn-danger mt-3">Confirmar</a>
            </div>
        </div>
    </div>
</div>
