<?php
require_once "../functions/products.php";
require_once "../functions/utilities.php";

if (isset($_SESSION['success'])) {
    $success = $_SESSION['success'];
    unset($_SESSION['success']);
}

$categories = [];
$categories["Accion"] = get_products_by_category($db, "Acción");
$categories["Aventura"] = get_products_by_category($db, "Aventura");
$categories["Deportes"] = get_products_by_category($db, "Deportes");
$categories["Estrategia"] = get_products_by_category($db, "Estrategia");
$categories["Indie"] = get_products_by_category($db, "Indie");
$categories["RPG"] = get_products_by_category($db, "RPG");
$categories["Shooter"] = get_products_by_category($db, "Shooter");
$categories["Survival Horror"] = get_products_by_category($db, "Survival Horror");
?>
<div class="row mt-3 listado-productos">
    <div class="col">
        <?php if (isset($success) && $success != "") : ?>
            <div class="row">
                <div class="alert alert-success w-100 mt-3" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Descartar mensaje">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="alert-heading">¡Felicitaciones!</h4>
                    <p><?= $success; ?></p>
                </div>
            </div>
        <?php
        endif;
        foreach ($categories as $category) : ?>
            <section class="row">
                <div class="col-12 mt-3">
                    <h3><?= $category[0]['category'] ?? ''; ?></h3>
                </div>
                <?php for ($i = 0; $i < count($category); $i++) : ?>
                    <article class="col-12 col-md-6 col-lg-4 mb-4">
                        <div class="card h-100">
                            <picture>
                                <source srcset="../assets/img/desktop-<?= $category[$i]['image'] ?>" media="(min-width: 1200px)">
                                <source srcset="../assets/img/tablet-<?= $category[$i]['image'] ?>" media="(min-width: 768px)">
                                <img src="../assets/img/phone-<?= $category[$i]['image'] ?>" class="card-img-top" alt="<?= $category[$i]['image_desc'] ?>">
                            </picture>
                            <div class="card-body">
                                <div>
                                    <h4 class="card-title"><?= $category[$i]['name']; ?></h4>
                                    <p class="card-text"><?= excerpt($category[$i]['description'], 100); ?></p>
                                    
                                    <?php if ($category[$i]['price'] <= 50): ?>
                                        <div><span class="highlight-tag oferta">¡SÚPER OFERTA!</span></div>
                                    <?php endif; ?>
                                    
                                    <?php if ($category[$i]['stock'] > 0 && $category[$i]['stock'] <= 100): ?>
                                        <div><span class="highlight-tag last-units">ÚLTIMAS UNIDADES</span></div>
                                    <?php endif; ?>

                                    <?php if ($category[$i]['stock'] == 0): ?>
                                        <div><span class="highlight-tag out-of-stock">SIN STOCK</span></div>
                                    <?php endif; ?>
                                    
                                </div>
                                <div class="editar-borrar">
                                    <a href="index.php?v=editar-producto&id=<?= $category[$i]['id_product'] ?>" class="btn btn-warning mt-3 mr-2">Editar</a>
                                    <a href="index.php?v=borrar-producto&id=<?= $category[$i]['id_product'] ?>" class="btn btn-danger mt-3">Borrar</a>
                                </div>
                            </div>
                        </div>
                    </article>
                <?php endfor; ?>
            </section>
        <?php endforeach; ?>
    </div>
</div>
