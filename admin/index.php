<?php
require_once '../bootstrap/connection.php';
require_once '../functions/authenticate.php';
require_once '../functions/utilities.php';

$view = $_GET['v'] ?? 'home';

$allowed_sections = [
    'home' => [
        'title' => 'Bienvenido admin',
        'auth' => true
    ],
    'nuevo-producto' => [
        'title' => 'Cargar producto',
        'auth' => true
    ],
    'productos' => [
        'title' => 'Lista de productos',
        'auth' => true
    ],
    'editar-producto' => [
        'title' => 'Editar producto',
        'auth' => true
    ],
    'borrar-producto' => [
        'title' => 'Borrar producto',
        'auth' => true
    ],
    'login' => [
        'title' => 'Zona de administración',
        'auth' => false
    ],
    '404' => [
        'title' => 'Oops! Página no encontrada',
        'auth' => false
    ],
];

if (!isset($allowed_sections[$view])) {
    $view = "404";
}

if ($allowed_sections[$view]['auth']) {
    if (!is_authenticated() || !is_admin()) {
        header('Location: ../index.php?v=home');
        exit;
    }
}

if (isset($_SESSION['success'])) {
    $success_msg = $_SESSION['success'];
    unset($_SESSION['success']);
} else {
    $success_msg = "";
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Playground ADMIN :: <?= $allowed_sections[$view]['title']; ?></title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css">
    <link rel="icon" href="../assets/img/favico.png">
</head>

<body>
    <!-- HEADER -->
    <?php require_once 'components/header.php'; ?>

    <!-- MAIN -->
    <main class="main-wrapper">
        <?php
        if (file_exists('sections/' . $view . '.php') && $view != '404') {
            require 'sections/' . $view . '.php';
        } else if (file_exists('sections/404.php') && $view == '404') {
            require 'sections/404.php';
        } else {
            require 'sections/mantenimiento.php';
        }
        ?>
    </main>
    <?php require_once '../components/footer.php'; ?>
    <script src="../js/jquery-3.4.1.min.js"></script>
    <script src="../js/bootstrap.bundle.min.js"></script>
    <script src="../js/header.js"></script>
    <script src="../js/functions.js"></script>
</body>

</html>
