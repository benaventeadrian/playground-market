<?php

require '../../bootstrap/connection.php';
require '../../functions/authenticate.php';

$email = $_POST['email'];
$password = $_POST['password'];

$user = login($db, $email, $password);

if($user) {
    if($user['id_role'] == 1) {
        header("Location: ../index.php?v=home");
    } else {
        header("Location: ../../index.php?v=home");
    }

} else {
    $_SESSION['errors'] = [
        'credentials' => 'Las credenciales ingresadas son incorrectas. ¿Estás registrado/a?'
    ];
    $_SESSION['old_data'] = $_POST;
    header("Location: ../../index.php?v=login");
}

