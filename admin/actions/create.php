<?php

require '../../bootstrap/connection.php';
require '../../functions/products.php';
require '../../functions/image.php';

$title          = $_POST['title'];
$description    = $_POST['description'];
$price          = $_POST['price'];
$stock          = $_POST['stock'];
$id_category    = $_POST['category'];
$cover          = $_FILES['cover'];
$cover_desc     = $_POST['cover_desc'];

$errors = [];

if ($title == "") {
    $errors['title'] = "El título no puede estar vacío.";
} else if (strlen($title) < 3) {
    $errors['title'] = "El título debe tener al menos 3 caracteres.";
} else if (strlen($title) > 500) {
    $errors['title'] = "El título no puede tener más de 500 caracteres.";
}

if ($description == "") {
    $errors['description'] = "El texto de descripción no puede estar vacío.";
} else if (strlen($description) > 2000) {
    $errors['description'] = "El texto de descripción no puede tener más de 2000 caracteres.";
} else if (strlen($description) < 10) {
    $errors['description'] = "El texto de descripción no puede tener menos de 10 caracteres.";
}

if ($price == "") {
    $errors['price'] = "El campo 'precio' no puede estar vacío.";
} else if (strlen($price) > 6) {
    $errors['price'] = "El precio del producto no puede tener más de 5 cifras.";
} else if (!is_numeric($price)) {
    $errors['price'] = "El campo 'precio' solo puede contener caracteres de tipo numérico.";
}

if ($stock == "") {
    $errors['stock'] = "El campo 'stock' no puede estar vacío.";
} else if (strlen($stock) > 8) {
    $errors['stock'] = "El campo 'stock' no puede contener más de 8 cifras.";
} else if (!is_numeric($stock)) {
    $errors['stock'] = "El campo 'stock' solo puede contener caracteres de tipo numérico.";
}

if ($id_category == "") {
    $errors['category'] = "Debes seleccionar una categoría.";
}

if (!empty($cover['tmp_name'])) {
    $image_path = '../../assets/img/';
    $image_name = get_filename($cover['name']);
    $image_extension = get_extension($cover['name']);
    $image_filename = $image_name . '.' . $image_extension;

    $images_names = generate_cover_images($cover, $image_path, $image_name, true);

    $image_versions = [
        'phone' => $images_names['phone'],
        'tablet' => $images_names['tablet'],
        'desktop' => $images_names['desktop']
    ];

    foreach ($image_versions as $image) {
        move_uploaded_file($cover['tmp_name'], $image_path . $image);
    }
} else {
    $image_filename = '';
}

if (count($errors) > 0) {
    $_SESSION['errors'] = $errors;
    $_SESSION['old_data'] = $_POST;

    header("Location: ../index.php?v=nuevo-producto");
    exit;
}

$success = create_product($db, [
    'title' => $title,
    'description' => $description,
    'price' => $price,
    'stock' => $stock,
    'id_category' => $id_category,
    'cover' => $image_filename,
    'cover_desc' => $cover_desc
]);

if ($success) {
    $_SESSION['success'] = "El producto se cargó exitosamente.";
    header("Location: ../index.php?v=home");
} else {
    $_SESSION['errors'] = "Error del servidor. Por favor reintentalo más tarde.";
    header("Location: ../index.php?v=nuevo-producto");
}
