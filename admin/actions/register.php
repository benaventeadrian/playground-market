<?php

require_once "../../bootstrap/connection.php";
require_once "../../functions/user.php";

$email = $_POST['email_register'];
$password = $_POST['password_register'];
$password_rep = $_POST['password_repeat'];

$errors = [];

if($email == "") {
    $errors['email_register'] = "El email no puede estar vacío.";
} else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $errors['email_register'] = "El email no parece tener un formato válido.";
} else if(get_user_by_email($db, $email)) {
    $errors['email_register'] = "El email ingresado ya está en uso. Por favor, seleccioná otro o simplemente logueate";
}

if($password == "") {
    $errors['password_register'] = "La contraseña no puede estar vacía.";
} else if(strlen($password) < 3) {
    $errors['password_register'] = "La contraseña debe tener al menos 3 caracteres.";
} else if($password !== $password_rep) {
    $errors['password_repeat'] = "Las contraseñas no coinciden.";
}

if(count($errors) > 0) {
    $_SESSION['errors'] = $errors;
    $_SESSION['old_data'] = $_POST;
    header('Location: ../../index.php?v=login');
    exit;
}

$success = register_user($db, [
    'email' => $email,
    'password' => password_hash($password, PASSWORD_DEFAULT),
    'id_role' => 2
]);

if($success) {
    $_SESSION['success'] = "Te has registrado exitosamente. Accede para poder comprar.";
    header("Location: ../../index.php?v=home");
} else {
    $_SESSION['errors'] = "Error del servidor. Por favor, intentalo de nuevo más tarde.";
    $_SESSION['old_data'] = $_POST;
    header("Location: ../../index.php?v=login");
}




