<header class="container-fluid">
    <div class="container">
        <button type="button" class="burger">Menu</button>
        <h1 id="logo"><a href="index.php?v=home">Playgroundet</a></h1>
        <?php
        require_once '../functions/authenticate.php';
        if (is_authenticated()) :
            ?>
            <nav class="main-menu">
                <div class="container">
                    <ul>
                        <li><a href="index.php?v=home" <?= $view == 'home' ? 'class="active"' : '' ?>>Home</a></li>
                        <li><a href="index.php?v=nuevo-producto" <?= $view == 'nuevo-producto' ? 'class="active"' : '' ?>>Nuevo Producto</a></li>
                        <li><a href="index.php?v=productos" <?= $view == 'productos' ? 'class="active"' : '' ?>>Productos</a></li>
                        <li><a href="actions/logout.php">Salir</a></li>
                    </ul>
                </div>
            </nav>
        <?php
        endif;
        ?>
    </div>
</header>
