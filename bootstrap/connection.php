<?php

session_start();

const DB_HOST = 'localhost';
const DB_USER = 'root';
const DB_PASS = '';
const DB_BASE = 'playground';

$db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_BASE);

if(!$db) {
    require '../views/mantenimiento.php';
    exit;
}

mysqli_set_charset($db, 'utf8mb4');
