<?php

function add_to_cart($db, $id_product, $quantity, $id_user) {
    mysqli_query($db, "START TRANSACTION");

    $id_product = mysqli_real_escape_string($db, $id_product);
    $quantity = mysqli_real_escape_string($db, $quantity);
    $id_user = mysqli_real_escape_string($db, $id_user);

    $query = "INSERT INTO cart (
        id_user,
        id_product,
        quantity
    ) VALUES (
        '$id_user',
        '$id_product',
        '$quantity'
    )";

    $success = mysqli_query($db, $query);

    if($success) {
        mysqli_query($db, "COMMIT");
        return true;
    } else {
        mysqli_query($db, "ROLLBACK");
        return false;
    }
}

function remove_from_cart($db, $id_user, $id_product = null) {
    mysqli_query($db, "START TRANSACTION");

    $id_user = mysqli_real_escape_string($db, $id_user);
    $id_product = mysqli_real_escape_string($db, $id_product);

    $query = "DELETE FROM cart 
                WHERE id_user = '$id_user'";

    if ($id_product != null) {
        $query .= "AND id_product = '$id_product'";
    }

    $success = mysqli_query($db, $query);

    if ($success) {
        mysqli_query($db, "COMMIT");
        return true;
    } else {
        mysqli_query($db, "ROLLBACK");
        return false;
    }
}

function get_user_cart($db, $id_user) {
    $id_user = mysqli_real_escape_string($db, $id_user);

    $query = "SELECT p.id_product, SUM(quantity) AS total
                FROM cart c
                LEFT JOIN products p ON c.id_product = p.id_product
                WHERE c.id_user = '$id_user'
                GROUP BY id_product";

    $res = mysqli_query($db, $query);

    while ($row = mysqli_fetch_assoc($res)) {
        $cart_list[] = $row;
    }
    
    return $cart_list ?? [];
}
