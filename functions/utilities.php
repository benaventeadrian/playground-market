<?php
function excerpt($text, $length) {
  if (strlen($text) <= $length) {
    return $text;
  } else {
    $excerpt = substr($text, 0, $length) . "...";
    return $excerpt;
  }
}

function is_authenticated() {
    return isset($_SESSION['active_user']);
}

function is_admin() {
    if (is_authenticated()) {
        return $_SESSION['active_user']['id_role'] == 1;
    } else {
        return "No hay ningún usuario logueado";
    }
}





