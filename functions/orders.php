<?php

function create_order($db, $data) {
    mysqli_query($db, "START TRANSACTION");

    $id_user = mysqli_real_escape_string($db, $data['id_user']);
    $id_product = mysqli_real_escape_string($db, $data['id_product']);
    $quantity = mysqli_real_escape_string($db, $data['quantity']);
    $address = mysqli_real_escape_string($db, $data['address']);
    $city = mysqli_real_escape_string($db, $data['city']);
    $zip_code = mysqli_real_escape_string($db, $data['zip_code']);
    $province = mysqli_real_escape_string($db, $data['province']);
    $payment_method = mysqli_real_escape_string($db, $data['payment_method']);

    $query = "INSERT INTO orders (
        id_user,
        id_product,
        quantity,
        user_address,
        city,
        zip_code,
        province,
        payment_method,
        date
    ) VALUES (
     '$id_user',   
     '$id_product',   
     '$quantity',   
     '$address',   
     '$city',   
     '$zip_code',   
     '$province',   
     '$payment_method',
     NOW()
    )";

    $success = mysqli_query($db, $query);

    if ($success) {
        mysqli_query($db, "COMMIT");
        return true;
    } else {
        mysqli_query($db, "ROLLBACK");
        return false;
    }
}

function get_latest_orders($db, $limit = null) {
    $query = "SELECT 
                    u.name as user,
                    p.name as game,
                    date
                FROM
                    orders o
                    LEFT JOIN users u ON o.id_user = u.id_user
                    LEFT JOIN products p ON o.id_product = p.id_product";

    if ($limit !== null) {
        $limit = mysqli_real_escape_string($db, $limit);
        $query .= " LIMIT " . $limit;
    }

    $res = mysqli_query($db, $query);

    $list = [];

    while($row = mysqli_fetch_assoc($res)) {
        $list[] = $row;
    }

    return $list ?? [];
}
