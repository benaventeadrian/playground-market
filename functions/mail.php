<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require "../../../vendors/PHPMailer/Exception.php";
require "../../../vendors/PHPMailer/PHPMailer.php";
require "../../../vendors/PHPMailer/SMTP.php";
require "../../../functions/user.php";
require "../../../functions/products.php";
require "../../../functions/vault_boy_success.php";

function email_order_details($db, $data) {
    $user = get_user_by_id($db, $data['id_user']);
    $products = $data['id_product'];
    $quantity = $data['quantity'];
    $price = 0;
    $times = 0;
    
    $mail = new PHPMailer(true);

    try {
        //Server settings
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host       = "smtp.gmail.com";
        $mail->SMTPAuth   = true;
        $mail->Username   = "adrian.benavente@davinci.edu.ar";
        $mail->Password   = "31344463";
        $mail->SMTPSecure = "tls";
        $mail->Port       = 587;

        //Recipients
        $mail->setFrom("adrian.benavente@davinci.edu.ar", "Playground Admin");
        $mail->addAddress($user['email']);

        // Content
        $mail->isHTML(true);
        $mail->Subject = "Pedido recibido de " . $user['email'];
        $mail->Body    = "
            <h1>Hola " . $user['name'] . "!, recibimos tu pedido:</h1>
            <p>A continuación los detalles del mismo:</p>
            <div>
                <table style='border-collapse: collapse; border: 1px solid black;'>
                    <tr>
                        <th style='text-align: left; border: 1px solid black; padding: 8px;'>Producto</th>
                        <th style='text-align: left; border: 1px solid black; padding: 8px;'>Cantidad</th>
                        <th style='text-align: left; border: 1px solid black; padding: 8px;'>Precio</th>
                    </tr>";
                    for ($i = 0;$i < count($products);$i++) {
                        $total = 0;
                        $total += $quantity[$i];
                        $times += $total;

                        $mail->Body .= "<tr>";
                        $mail->Body .= "<td style='border: 1px solid black; padding: 8px;'>" . get_product_by_id($db, $products[$i])['name'] . "</td>";
                        $mail->Body .= "<td style='border: 1px solid black; padding: 8px;'>" . $total . "</td>";
                        $mail->Body .= "<td style='border: 1px solid black; padding: 8px;'>" . get_product_by_id($db, $products[$i])['price'] . "</td>";
                        $mail->Body .= "</tr>";
                    }
                    if (count($products) > 1) {
                        foreach ($products as $product) {
                            $price += get_product_by_id($db, $product)['price'];
                        }
                    } else {
                        foreach ($products as $product) {
                            $price = get_product_by_id($db, $product)['price'] * $times; 
                        }
                    }
                    $mail->Body .= "
                    <tr>
                        <td colspan='3' style='padding: 8px; text-align: right;'>Total: $". $price . "</td>
                    </tr>
                </table>
            </div>
            <div>
                <img src='" . vault_boy_success_b64_img() . "' style='max-width: 300px; display: block;' alt='Vault Boy contento porque tu pedido fue realizado con éxito'>
            </div>
        ";

        if ($mail->send()) {
            return true;
        } else {
            return false;
        }
    } catch (Exception $e) {
        echo "Hubo un error al enviar el mensaje: {$mail->ErrorInfo}";
    }
}

function email_contact($user_email) {
    $mail = new PHPMailer(true);

    try {
        //Server settings
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host       = "smtp.gmail.com";
        $mail->SMTPAuth   = true;
        $mail->Username   = "adrian.benavente@davinci.edu.ar";
        $mail->Password   = "31344463";
        $mail->SMTPSecure = "tls";
        $mail->Port       = 587;

        //Recipients
        $mail->setFrom("adrian.benavente@davinci.edu.ar", "Playground Admin");
        $mail->addAddress($user_email);

        // Content
        $mail->isHTML(true);
        $mail->Subject = "Recibimos tu consulta";
        $mail->Body    = "
            <h1>¡Genial!</h1>
            <p>Recibimos tu consulta, nos pondremos en contacto con vos a la brevedad.</p>
        ";

        if ($mail->send()) {
            return true;
        } else {
            return false;
        }
    } catch (Exception $e) {
        echo "Hubo un error al enviar el mensaje: {$mail->ErrorInfo}";
    }
}
