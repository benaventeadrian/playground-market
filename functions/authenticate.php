<?php

function login($db, $email, $password) {
    $email = mysqli_real_escape_string($db, $email);
    
    $query = "SELECT * FROM users
            WHERE email = '$email'";
    $res = mysqli_query($db, $query);

    if($user_set = mysqli_fetch_assoc($res)) {
        if(password_verify($password, $user_set['password'])) {
            $_SESSION['active_user']['id_user'] = $user_set['id_user'];
            $_SESSION['active_user']['email'] = $user_set['email'];
            $_SESSION['active_user']['username'] = $user_set['username'];
			$_SESSION['active_user']['id_role'] = $user_set['id_role'];
            
            $active_user = $_SESSION['active_user'];
            
            return $active_user;
        }
	}
	
    return false;
}

function logout() {
    unset($_SESSION['active_user']);
}
