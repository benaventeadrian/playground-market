<?php
function get_user_by_id($db, $id_user) {
    $id_user = mysqli_real_escape_string($db, $id_user);
    
    $query = "SELECT id_user, email, name, last_name, avatar, id_role FROM users
            WHERE id_user = '$id_user'";
    $res = mysqli_query($db, $query);
    
    $row = mysqli_fetch_assoc($res);
    
    if($row) {
        return $row;
    }

    return null;
}

function get_user_role($db, $id_role) {
    $id_role = mysqli_real_escape_string($db, $id_role);

    $query = "SELECT name FROM roles WHERE id_role = '$id_role'";

    $res = mysqli_query($db, $query);
    
    $role = mysqli_fetch_assoc($res);

    return $role['name'];
}

function get_user_by_email($db, $email) {
    $email = mysqli_real_escape_string($db, $email);
    
    $query = "SELECT id_user, email, name, last_name, avatar, id_role FROM users
            WHERE email = '$email'";
    $res = mysqli_query($db, $query);
    
    $row = mysqli_fetch_assoc($res);
    
    if($row) {
        return $row;
    }

    return null;
}

function register_user($db, $data) {
    $email = mysqli_real_escape_string($db, $data['email']);
    $password = mysqli_real_escape_string($db, $data['password']);
    $id_role = mysqli_real_escape_string($db, $data['id_role']);
    
    $query = "INSERT INTO users (password, email, id_role)
            VALUES ('$password', '$email', '$id_role')";

    $res = mysqli_query($db, $query);

    return $res;
}

function set_user_name($db, $id_user, $name) {
    mysqli_query($db, "START TRANSACTION");

    $id_user = mysqli_real_escape_string($db, $id_user);
    $name = mysqli_real_escape_string($db, $name);

    $query = "UPDATE users SET name = '$name' WHERE id_user = '$id_user'";
    $success = mysqli_query($db, $query);

    if($success) {
        mysqli_query($db, "COMMIT");
        return true;
    } else {
        mysqli_query($db, "ROLLBACK");
        return false;
    }
}

function set_user_lastname($db, $id_user, $last_name) {
    mysqli_query($db, "START TRANSACTION");

    $id_user = mysqli_real_escape_string($db, $id_user);
    $last_name = mysqli_real_escape_string($db, $last_name);

    $query = "UPDATE users SET last_name = '$last_name' WHERE id_user = '$id_user'";
    $success = mysqli_query($db, $query);

    if($success) {
        mysqli_query($db, "COMMIT");
        return true;
    } else {
        mysqli_query($db, "ROLLBACK");
        return false;
    }
}

function set_user_avatar($db, $id_user, $filename) {
    mysqli_query($db, "START TRANSACTION");

    $id_user = mysqli_real_escape_string($db, $id_user);
    $filename = mysqli_real_escape_string($db, $filename);

    $query = "UPDATE users SET avatar = '$filename' WHERE id_user = '$id_user'";
    $success = mysqli_query($db, $query);

    if($success) {
        mysqli_query($db, "COMMIT");
        return true;
    } else {
        mysqli_query($db, "ROLLBACK");
        return false;
    }
}
