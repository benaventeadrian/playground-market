<?php

/* ALTA */
function create_product($db, $data) {
    mysqli_query($db, "START TRANSACTION");

    $id_category = mysqli_real_escape_string($db, $data['id_category']);
    $title = mysqli_real_escape_string($db, $data['title']);
    $description = mysqli_real_escape_string($db, $data['description']);
    $price = mysqli_real_escape_string($db, $data['price']);
    $stock = mysqli_real_escape_string($db, $data['stock']);
    $cover = mysqli_real_escape_string($db, $data['cover']);
    $cover_desc = mysqli_real_escape_string($db, $data['cover_desc']);

    $query = "INSERT INTO products (
        id_category, 
        name, 
        description, 
        price, 
        stock, 
        image, 
        image_desc
    ) VALUES (
        '$id_category', 
        '$title', 
        '$description', 
        '$price', 
        '$stock', 
        '$cover', 
        '$cover_desc'
    )";

    $success = mysqli_query($db, $query);

    if($success) {
        mysqli_query($db, "COMMIT");
        return true;
    } else {
        mysqli_query($db, 'ROLLBACK');
        return false;
    }
}

/* BAJA */
function delete_product($db, $id) {
    mysqli_query($db, "START TRANSACTION");

    $id_product = mysqli_real_escape_string($db, $id);

    $query = "DELETE FROM products WHERE id_product = '$id_product'";

    $success = mysqli_query($db, $query);

    if($success) {
        mysqli_query($db, "COMMIT");
        return true;
    } else {
        mysqli_query($db, 'ROLLBACK');
        return false;
    }
}

/* MODIFICACIÓN */
function update_product($db, $data) {
    mysqli_query($db, "START TRANSACTION");

    $id_product = mysqli_real_escape_string($db, $data['id_product']);
    $title = mysqli_real_escape_string($db, $data['title']);
    $description = mysqli_real_escape_string($db, $data['description']);
    $price = mysqli_real_escape_string($db, $data['price']);
    $stock = mysqli_real_escape_string($db, $data['stock']);
    $id_category = mysqli_real_escape_string($db, $data['category']);
    $cover = mysqli_real_escape_string($db, $data['cover']);
    $cover_desc = mysqli_real_escape_string($db, $data['cover_desc']);

    $query = "UPDATE products 
                SET id_category = '$id_category',
                    name = '$title',
                    description = '$description',
                    price = '$price',
                    stock = '$stock',
                    image = '$cover',
                    image_desc = '$cover_desc'
                WHERE id_product = '$id_product'";

    $success = mysqli_query($db, $query);

    if($success) {
        mysqli_query($db, "COMMIT");
        return true;
    } else {
        mysqli_query($db, "ROLLBACK");
        return false;
    }
}

function update_stock($db, $id_product, $quantity) {
    $id_product = mysqli_real_escape_string($db, $id_product);
    $quantity = mysqli_real_escape_string($db, $quantity);

    $new_stock = get_product_stock($db, $id_product) - $quantity;

    mysqli_query($db, "START TRANSACTION");

    $query = "UPDATE products 
                SET stock = '$new_stock'
                WHERE id_product = '$id_product'";

    $success = mysqli_query($db, $query);

    if ($success) {
        mysqli_query($db, "COMMIT");
        return true;
    } else {
        mysqli_query($db, "ROLLBACK");
        // return false;
        return mysqli_errno($db) . ": " . mysqli_error($db);
    }
}

/* LECTURA */
function get_products_list($db, $limit = null) {
    $query = "SELECT p.*,
                GROUP_CONCAT(c.name SEPARATOR ' - ') AS categories
                FROM products p
                LEFT JOIN categories c ON c.id_category = p.id_category
                GROUP BY p.id_product
                ORDER BY categories";

    if($limit !== null) {
        $limit = mysqli_real_escape_string($db, $limit);
        $query .= " LIMIT " . $limit;
    }

    $res = mysqli_query($db, $query);
    $list = [];

    while ($row = mysqli_fetch_assoc($res)) {
        $list[] = $row;
    }

    return $list;
}

function get_products_by_category($db, $category) {
    $category = mysqli_real_escape_string($db, $category);
    $query = "SELECT p.*,
                GROUP_CONCAT(c.name) AS category
                FROM products p
                LEFT JOIN categories c ON c.id_category = p.id_category
                WHERE c.name = '$category'
                GROUP BY p.name";

    $res = mysqli_query($db, $query);
    $list = [];

    while ($row = mysqli_fetch_assoc($res)) {
        $list[] = $row;
    }

    return $list;
}

function get_product_by_id($db, $id_product) {
    $id_product = mysqli_real_escape_string($db, $id_product);
    $query = "SELECT * FROM products
                WHERE id_product = '$id_product'";

    $res = mysqli_query($db, $query);
    $product = mysqli_fetch_assoc($res);

    return $product;
}

function get_product_stock($db, $id_product) {
    $id_product = mysqli_real_escape_string($db, $id_product);

    $query = "SELECT stock FROM products 
                WHERE id_product = '$id_product'";

    $res = mysqli_query($db, $query);

    $success = mysqli_fetch_assoc($res);

    return $success['stock'];
}

function get_latest_products($db, $limit = null) {
    $query = "SELECT p.*
            FROM products p
            ORDER BY id_product DESC";

    if ($limit !== null) {
        $limit = mysqli_real_escape_string($db, $limit);
        $query .= " LIMIT " . $limit;
    }

    $res = mysqli_query($db, $query);

    $list = [];

    while($row = mysqli_fetch_assoc($res)) {
        $list[] = $row;
    }

    return $list;
}

function count_products($db) {
    $query = "SELECT COUNT(id_product) AS number_of_products FROM products";

    $res = mysqli_query($db, $query);

    $count = mysqli_fetch_assoc($res);

    return $count['number_of_products'];
}

function get_categories($db) {
    $query = "SELECT * FROM categories";

    $res = mysqli_query($db, $query);

    $list = [];

    while ($row = mysqli_fetch_assoc($res)) {
        $list[] = $row;
    }

    return $list;
}

function get_category_id($db, $name) {
    $name = mysqli_real_escape_string($db, $name);

    $query = "SELECT c.id_category FROM categories c
                WHERE name = '$name'";

    $res = mysqli_query($db, $query);

    $category_id = mysqli_fetch_assoc($res);

    return $category_id['id_category'];
}

function get_category_by_id($db, $id) {
    $id = mysqli_real_escape_string($db, $id);
    $query = "SELECT c.name FROM categories c
                WHERE id_category = '$id'";

    $res = mysqli_query($db, $query);
    $category = mysqli_fetch_assoc($res);

    return $category['name'];
}
