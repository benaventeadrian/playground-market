<?php

function get_extension($filename) {
    return substr($filename, strrpos($filename, '.') + 1);
}


function get_filename($filename) {
    return explode(".", $filename)[0];
}


function create_image($image_path, $extension = null) {
    $extension = $extension ?? get_extension($image_path);

    switch($extension) {
        case 'jpg':
        case 'jpeg':
            return imagecreatefromjpeg($image_path);
            break;

        case 'gif':
            return imagecreatefromgif($image_path);
            break;

        case 'png':
            return imagecreatefrompng($image_path);
            break;

        default:
            return false;
    }
}


function save_image($image_path, $file_path, $extension = null) {
    $extension = $extension ?? get_extension($file_path);

    switch($extension) {
        case 'jpg':
        case 'jpeg':
            return imagejpeg($image_path, $file_path);
            break;

        case 'gif':
            return imagegif($image_path, $file_path);
            break;

        case 'png':
            return imagepng($image_path, $file_path);
            break;

        default:
            return false;
    }
}


function crop_image($image, $width, $height) {
    $imageX = imagesx($image);
    $imageY = imagesy($image);
    if($imageY <= $height && $imageX <= $width) return $image;
    
    $startX = ($imageX / 2) - ($width / 2);
    $startY = ($imageY / 2) - ($height / 2);

    $cropped = imagecrop($image, [
        'x' => $startX,
        'y' => $startY,
        'width' => $width,
        'height' => $height
    ]);

    return $cropped === false ? $image : $cropped;
}


function generate_cover_images($image, $path, $filename = null, $crop = false) {
    $filename = $filename ?? time();
    $extension = get_extension($image['name']);
    $image_copy = create_image($image['tmp_name'], $extension);

    $image_phone = imagescale($image_copy, 576);
    $image_tablet = imagescale($image_copy, 768);
    $image_desktop = imagescale($image_copy, 1280);

    if($crop) {
        [$image_phone, $image_tablet, $image_desktop] = generate_cropped_images($image_phone, $image_tablet, $image_desktop);
    }

    $resized_phone_name = "phone-" . $filename . "." . $extension;
    $resized_tablet_name = "tablet-" . $filename . "." . $extension;
    $resized_desktop_name = "desktop-" . $filename . "." . $extension;

    save_image($image_phone, $path . $resized_phone_name);
    save_image($image_tablet, $path . $resized_tablet_name);
    save_image($image_desktop, $path . $resized_desktop_name);

    return [
        'phone' => $resized_phone_name, 
        'tablet' => $resized_tablet_name, 
        'desktop' => $resized_desktop_name
    ];
}


function generate_cropped_images($phone, $tablet, $desktop) {
    return [
        crop_image($phone, 576, 324),
        crop_image($tablet, 768, 432),
        crop_image($desktop, 1280, 720)
    ];
}
