<?php
require_once 'bootstrap/connection.php';
require_once 'functions/utilities.php';

$view = $_GET['v'] ?? 'home';

$allowed_sections = [
    'home' => [
        'title' => 'La tienda que cabe en tu bolsillo'
    ],
    'productos' => [
        'title' => 'Nuestros productos más vendidos'
    ],
    'detalle' => [
        'title' => 'Detalle de producto'
    ],
    'contacto' => [
        'title' => 'Contactate con nosotros'
    ],
    'perfil' => [
        'title' => 'Perfil de usuario'
    ],
    'carrito' => [
        'title' => 'Carrito de compras'
    ],
    'comprar' => [
        'title' => 'Completá tu pedido'
    ],
    'login' => [
        'title' => 'Ingreso de usuario'
    ],
    'thankyou' => [
        'title' => 'Gracias por contactarnos'
    ],
    'mantenimiento' => [
        'title' => 'Sitio en mantenimiento'
    ],
    '404' => [
        'title' => 'Oops! Página no encontrada'
    ],
];

if (!isset($allowed_sections[$view])) {
    $view = "404";
}

if (is_authenticated() && is_admin()) {
    header("Location: admin/index.php?v=home");
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Playground - <?= $allowed_sections[$view]['title'] ?></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="icon" href="assets/img/favico.png">
</head>

<body>
    <?php require_once 'components/header.php'; ?>
    <main>
        <?php
            if (file_exists('views/' . $view . '.php')) {
                require 'views/' . $view . '.php';
            } else {
                require 'views/mantenimiento.php';
            }
        ?>
    </main>
    <?php require_once 'components/footer.php'; ?>
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/header.js"></script>
    <script src="js/users.js"></script>
    <script>
        $('.carousel').carousel()
    </script>
</body>

</html>
