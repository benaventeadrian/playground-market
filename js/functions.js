window.addEventListener('DOMContentLoaded', () => {
    let productPrice = document.querySelector('.form #price');
    let productStock = document.querySelector('.form #stock');

    inputOnlyNumbers(productPrice);
    inputOnlyNumbers(productStock);


    /**
     * Evita que se ingrese cualquier caracter no numérico
     * en un campo de formulario
     * @param {object} elem (Elemento HTML cuya interacción queremos escuchar)
     * @return {void}
     */
    function inputOnlyNumbers(elem) {
        elem.addEventListener('keypress', e => {
            if (e.which < 48 || e.which > 57) {
                e.preventDefault();
            }
        });

        elem.addEventListener('change', e => {
            if (isNaN(e.target.value)) {
                e.target.value = '';
            }
        });
    }
});
