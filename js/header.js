const burgerBtn = document.querySelector('.burger');
const header = document.querySelector('header');
const menu = document.querySelector('header nav');

burgerBtn.addEventListener('click', () => {
  const mql = window.matchMedia('(max-width: 991px)');

  menu.classList.toggle('open');

  if (mql.matches) {
    menu.style.top = `${header.offsetHeight}px`;
  }
});

window.addEventListener('scroll', () => {
  menu.classList.remove('open');

  if (window.pageYOffset > header.offsetHeight) {
    header.classList.remove('unpinned');
    header.classList.add('pinned');
  } else {
    header.classList.remove('pinned');
    header.classList.add('unpinned');
  }
});

window.matchMedia('(max-width: 991px)').addListener(e => {
  if (e.matches) {
    menu.style.top = `${header.offsetHeight}px`;
  } else {
    menu.style.top = 0;
  }
});
