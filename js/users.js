window.addEventListener("DOMContentLoaded", () => {
    const inputUserName = document.querySelector("#user-profile input#name");
    const inputUserLastName = document.querySelector(
        "#user-profile input#last_name"
    );
    const editName = document.querySelector("#edit-name");
    const editLastName = document.querySelector("#edit-lastname");
    
    const editRelatedField = field => {
        const relatedField = field.parentElement.previousElementSibling.lastElementChild;
        relatedField.removeAttribute("readonly");
        relatedField.focus();
    };
    
    editName.addEventListener('click', e => editRelatedField(e.target));
    editLastName.addEventListener('click', e => editRelatedField(e.target));
});
